OS = $(shell uname | tr A-Z a-z)

# Dependency versions
GOTESTSUM_VERSION=1.10.0

.PHONY: build test clean test

all: clean build run

lint: bin/golangci-lint tidy fmt vet
	@./bin/golangci-lint run

vet:
	@go vet ./...

fmt:
	@go fmt ./...

run: clean build
	./gitlab-code-suggestions-language-server-experiment serve --name code-completion-lsp --srcdir . --host "localhost" --timeout-seconds 120

build:
	go build

tidy:
	@go mod tidy

clean:
	- rm -f ./gitlab-code-suggestions-language-server-experiment

test: bin/gotestsum-${GOTESTSUM_VERSION}
	@./bin/gotestsum-${GOTESTSUM_VERSION} --no-summary=skipped --junitfile ./coverage.xml --format short-verbose -- -coverprofile=./coverage.txt -covermode=atomic ./...

coverage:
	go tool cover -func coverage.txt

bin/gotestsum-${GOTESTSUM_VERSION}:
	@mkdir -p bin
	@curl -L https://github.com/gotestyourself/gotestsum/releases/download/v${GOTESTSUM_VERSION}/gotestsum_${GOTESTSUM_VERSION}_${OS}_amd64.tar.gz | tar -zOxf - gotestsum > ./bin/gotestsum-${GOTESTSUM_VERSION} && chmod +x ./bin/gotestsum-${GOTESTSUM_VERSION}

bin/golangci-lint:
	@mkdir -p bin
	@curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s
