package api

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestUser_IsValid(t *testing.T) {
	type fields struct {
		Username               string
		CodeSuggestionsEnabled bool
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
		want1  string
	}{
		{
			"Code Suggestions enabled",
			fields{"joe", true},
			true,
			"Code Suggestions: enabled",
		},
		{
			"Code Suggestions disabled",
			fields{"joe", false},
			false,
			"Code Suggestions: not enabled",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := User{
				Username:               tt.fields.Username,
				CodeSuggestionsEnabled: tt.fields.CodeSuggestionsEnabled,
			}

			got, got1 := u.IsValid()
			require.Equal(t, tt.want, got)
			require.Equal(t, tt.want1, got1)
		})
	}
}

func TestUser_IsTokenValidExplanation(t *testing.T) {
	type fields struct {
		Username               string
		CodeSuggestionsEnabled bool
		PersonalAccessToken    PersonalAccessToken
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
		want1  string
	}{
		{
			"Has no scopes",
			fields{"joe", false, PersonalAccessToken{"TOKEN", []string{""}}},
			false,
			"✖ Token 'TOKEN' is invalid - has scope(s) '[]' (needs 'api')",
		},
		{
			"Has unsuitable scopes",
			fields{"joe", false, PersonalAccessToken{"TOKEN", []string{"read_api"}}},
			false,
			"✖ Token 'TOKEN' is invalid - has scope(s) '[read_api]' (needs 'api')",
		},
		{
			"Has suitable scopes",
			fields{"joe", true, PersonalAccessToken{"TOKEN", []string{"api"}}},
			true,
			"✔ Token 'TOKEN' is valid - has scope(s) '[api]' (needs 'api'), Code Suggestions: enabled",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := User{
				Username:               tt.fields.Username,
				CodeSuggestionsEnabled: tt.fields.CodeSuggestionsEnabled,
				PersonalAccessToken:    tt.fields.PersonalAccessToken,
			}

			got, got1 := u.IsTokenValidExplanation()
			require.Equal(t, tt.want, got)
			require.Equal(t, tt.want1, got1)
		})
	}
}

func TestUser_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *User
		wantErr error
	}{
		{
			"Valid JSON",
			args{[]byte(`{ "data": { "currentUser": { "username": "joe", "ide": { "codeSuggestionsEnabled": true } } } }`)},
			&User{"joe", true, PersonalAccessToken{}},
			nil,
		},
		{
			"Empty response",
			args{[]byte(``)},
			&User{"", false, PersonalAccessToken{}},
			errors.New("unexpected end of JSON input"),
		},
		{
			"Invalid JSON",
			args{[]byte(`{`)},
			&User{"", false, PersonalAccessToken{}},
			errors.New("unexpected end of JSON input"),
		},
		{
			"JSON missing data",
			args{[]byte(`{}`)},
			&User{"", false, PersonalAccessToken{}},
			errors.New("JSON missing data"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{}
			err := u.UnmarshalJSON(tt.args.b)
			if err != nil {
				if tt.wantErr != nil {
					require.ErrorContains(t, err, tt.wantErr.Error())
				} else {
					require.FailNow(t, err.Error())
				}
			}
			require.Equal(t, tt.want, u)
		})
	}
}
