package api

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type codeSuggestionsToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
	CreatedAt   int64  `json:"created_at"`
}

type Client interface {
	GetCodeSuggestions(ctx context.Context, filePath, textBefore, textAfter string) (*CodeSuggestionResponse, error)
	GetCodeSuggestionsToken(ctx context.Context) (*codeSuggestionsToken, error)
}

type GitLabClient struct {
	clientName, clientVersion string
	httpClient                *http.Client
	currentToken              *codeSuggestionsToken
	baseURL                   string
}

func NewClient(clientName, clientVersion, token, baseURL string, timeout time.Duration) *GitLabClient {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConns:          1,
			IdleConnTimeout:       timeout,
			ResponseHeaderTimeout: timeout,
			DisableKeepAlives:     true,
		},
	}

	gitlabClient := &GitLabClient{
		baseURL:       baseURL,
		clientName:    clientName,
		clientVersion: clientVersion,
		httpClient:    client,
	}

	client.Transport = tokenTransport{
		Transport: client.Transport,
		Token:     token,
		Client:    gitlabClient,
	}

	return gitlabClient
}

type tokenTransport struct {
	Transport http.RoundTripper
	Token     string
	Client    *GitLabClient
}

func (att tokenTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.Header.Get("Authorization") == "" {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", att.Token))
	}

	userAgent := fmt.Sprintf("%s (%s:%s)",
		"code-completions-language-server-experiment",
		att.Client.clientName,
		att.Client.clientVersion)

	req.Header.Add("User-Agent", userAgent)

	return att.Transport.RoundTrip(req)
}

func (client *GitLabClient) SetClientNameAndVersion(clientName, clientVersion string) {
	log.Debugf("GitLabClient.SetClientNameAndVersion: %s, %s", clientName, clientVersion)
	client.clientName = clientName
	client.clientVersion = clientVersion
}

func (client *GitLabClient) GetUser(pat *PersonalAccessToken) (*User, error) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	query := map[string]string{
		"query": `
			{
				currentUser {
					username
					ide {
						codeSuggestionsEnabled
					}
				}
			}
    `,
	}
	queryAsJSON, err := json.Marshal(query)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/graphql", client.baseURL), bytes.NewBuffer(queryAsJSON))
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	res, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected %s response when acquiring token (response body: %q)", res.Status, string(body))
	}

	var u User
	err = json.Unmarshal(body, &u)
	if err != nil {
		return nil, err
	}

	u.PersonalAccessToken = *pat

	return &u, nil
}

func (client *GitLabClient) GetTokenSelf() (*PersonalAccessToken, error) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	req, err := http.NewRequest("GET", fmt.Sprintf("%s/api/v4/personal_access_tokens/self", client.baseURL), nil)
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)
	res, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected %s response when acquiring token (response body: %q)", res.Status, string(body))
	}

	var p PersonalAccessToken
	err = json.Unmarshal(body, &p)
	if err != nil {
		return nil, err
	}

	return &p, nil
}

func (client *GitLabClient) isTokenValid() bool {
	if client.currentToken != nil {
		currentTimeInUnix := time.Now().Unix()
		if client.currentToken.CreatedAt+client.currentToken.ExpiresIn >= currentTimeInUnix {
			return true
		}
	}

	return false
}

func (client *GitLabClient) ensureValidToken(ctx context.Context) error {
	if !client.isTokenValid() {
		token, err := client.GetCodeSuggestionsToken(ctx)
		if err != nil {
			return err
		}
		client.currentToken = token
	}

	return nil
}

func (client *GitLabClient) GetCodeSuggestionsToken(ctx context.Context) (*codeSuggestionsToken, error) {
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/v4/code_suggestions/tokens", client.baseURL), nil)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	res, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("unexpected %s response when acquiring token (response body: %q)", res.Status, string(body))
	}

	var token codeSuggestionsToken
	err = json.Unmarshal(body, &token)
	if err != nil {
		return nil, err
	}

	return &token, nil
}
