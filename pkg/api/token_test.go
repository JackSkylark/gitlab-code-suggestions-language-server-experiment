package api

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPersonalAccessToken_IsValid(t *testing.T) {
	type fields struct {
		Name   string
		Scopes []string
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
		want1  string
	}{
		{
			"Has sutiable scopes",
			fields{"TOKEN", []string{"api"}},
			true,
			"Token 'TOKEN' is valid - has scope(s) '[api]' (needs 'api')",
		},
		{
			"Has unsuitable scopes",
			fields{"TOKEN", []string{"read_api"}},
			false,
			"Token 'TOKEN' is invalid - has scope(s) '[read_api]' (needs 'api')",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PersonalAccessToken{
				Name:   tt.fields.Name,
				Scopes: tt.fields.Scopes,
			}

			got, got1 := p.IsValid()
			require.Equal(t, tt.want, got)
			require.Equal(t, tt.want1, got1)
		})
	}
}

func TestPersonalAccessToken_hasSuitableScopes(t *testing.T) {
	type fields struct {
		Name   string
		Scopes []string
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Has no scopes",
			fields{"TOKEN", []string{""}},
			false,
		},
		{
			"Has unsuitable scopes",
			fields{"TOKEN", []string{"read_api"}},
			false,
		},
		{
			"Has unsuitable scopes",
			fields{"TOKEN", []string{"read_api"}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PersonalAccessToken{
				Name:   tt.fields.Name,
				Scopes: tt.fields.Scopes,
			}

			got := p.hasSuitableScopes()
			require.Equal(t, tt.want, got)
		})
	}
}
