package api

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

type httpHandlerFunc func(*testing.T, *http.ServeMux)

func TestIntegrationGetCodeSuggestionsToken(t *testing.T) {
	token := os.Getenv("LANGSRV_TEST_GITLAB_TOKEN")
	if token == "" {
		t.Skip(`LANGSRV_TEST_GITLAB_TOKEN not set, skipping integration test`)
	}

	c := NewClient("TestIntegrationGetCodeSuggestionsToken", "0.0", token, "https://gitlab.com", time.Second*60)

	completionToken, err := c.GetCodeSuggestionsToken(context.Background())
	require.NoError(t, err)
	require.NotNil(t, completionToken)
	require.NotEmpty(t, completionToken.AccessToken)
}

func TestIsTokenValid(t *testing.T) {
	tt := []struct {
		description string
		createdAt   int64
		expected    bool
	}{
		{
			description: "when the token hasn't expired returns true",
			createdAt:   time.Now().Unix(),
			expected:    true,
		},
		{
			description: "when the token has expired returns false",
			createdAt:   time.Now().Unix() - 180,
			expected:    false,
		},
	}

	c := NewClient("TestIsTokenValid", "0.0", "123", "https://gitlab.com", time.Second*60)

	for _, test := range tt {
		t.Run(test.description, func(t *testing.T) {
			c.currentToken = &codeSuggestionsToken{
				AccessToken: "1234",
				ExpiresIn:   60,
				CreatedAt:   test.createdAt,
			}

			require.Equal(t, test.expected, c.isTokenValid())
		})
	}
}

func TestGitLabClient_GetUser(t *testing.T) {
	tests := []struct {
		name         string
		userResponse string
		want         *User
		wantErr      error
	}{
		{
			"Valid",
			`{ "data": { "currentUser": { "username": "joe", "ide": { "codeSuggestionsEnabled": true } } } }`,
			&User{Username: "joe", CodeSuggestionsEnabled: true, PersonalAccessToken: PersonalAccessToken{Name: "", Scopes: []string(nil)}},
			nil,
		},
		{
			"Empty",
			`{}`,
			nil,
			errors.New("JSON missing data"),
		},
		{
			"Invalid",
			``,
			nil,
			errors.New("unexpected end of JSON input"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := setupHTTPServer(t, func(t *testing.T, mux *http.ServeMux) {
				t.Helper()
				mux.HandleFunc("/api/graphql", func(w http.ResponseWriter, r *http.Request) {
					_, err := w.Write([]byte(tt.userResponse))
					if err != nil {
						t.Fatal(err)
					}
				})
			})
			defer ts.Close()

			c := NewClient("TestGitLabClient_GetUser", "0.0", "fake-token", ts.URL, time.Second*60)
			p := &PersonalAccessToken{}

			got, err := c.GetUser(p)
			if err != nil {
				if tt.wantErr != nil {
					require.ErrorContains(t, err, tt.wantErr.Error())
				} else {
					require.FailNow(t, err.Error())
				}
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestGitLabClient_GetTokenSelf(t *testing.T) {
	tests := []struct {
		name        string
		patResponse string
		want        *PersonalAccessToken
		wantErr     error
	}{
		{
			"Valid",
			`{ "name": "fake-token", "scopes": ["api"] }`,
			&PersonalAccessToken{Name: "fake-token", Scopes: []string{"api"}},
			nil,
		},
		{
			"Empty",
			`{}`,
			&PersonalAccessToken{},
			nil,
		},
		{
			"Invalid",
			``,
			nil,
			errors.New("unexpected end of JSON input"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := setupHTTPServer(t, func(t *testing.T, mux *http.ServeMux) {
				t.Helper()
				mux.HandleFunc("/api/v4/personal_access_tokens/self", func(w http.ResponseWriter, r *http.Request) {
					_, err := w.Write([]byte(tt.patResponse))
					if err != nil {
						t.Fatal(err)
					}
				})
			})
			defer ts.Close()

			c := NewClient("TestGitLabClient_GetTokenSelf", "0.0", "fake-token", ts.URL, time.Second*60)

			got, err := c.GetTokenSelf()
			if err != nil {
				if tt.wantErr != nil {
					require.ErrorContains(t, err, tt.wantErr.Error())
				} else {
					require.FailNow(t, err.Error())
				}
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func setupHTTPServer(t *testing.T, f httpHandlerFunc) *httptest.Server {
	t.Helper()

	mux := http.NewServeMux()
	f(t, mux)

	return httptest.NewServer(mux)
}
