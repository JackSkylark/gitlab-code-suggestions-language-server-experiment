package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

type User struct {
	Username               string `json:"data.currentUser.username"`
	CodeSuggestionsEnabled bool   `json:"data.currentUser.ide.codeSuggestionsEnabled"`
	PersonalAccessToken    PersonalAccessToken
}

func (u User) IsValid() (bool, string) {
	if u.CodeSuggestionsEnabled {
		return true, "Code Suggestions: enabled"
	} else {
		return false, "Code Suggestions: not enabled"
	}
}

func (u User) IsTokenValidExplanation() (bool, string) {
	valid := false
	result := "✖"
	patIsValid, patIsValidExplanation := u.PersonalAccessToken.IsValid()
	messages := []string{patIsValidExplanation}

	if patIsValid {
		var userIsValidExplanation string

		userIsValid, userIsValidExplanation := u.IsValid()
		messages = append(messages, userIsValidExplanation)

		if userIsValid {
			valid = true
			result = "✔"
		}
	}

	return valid, fmt.Sprintf("%s %s", result, strings.Join(messages, ", "))
}

func (u *User) UnmarshalJSON(b []byte) error {
	var f interface{}

	err := json.Unmarshal(b, &f)
	if err != nil {
		return err
	}

	m := f.(map[string]interface{})

	data, ok := m["data"].(map[string]interface{})
	if !ok || data == nil {
		return errors.New("JSON missing data")
	}

	currentUserData := data["currentUser"].(map[string]interface{})
	ideData := currentUserData["ide"].(map[string]interface{})

	u.Username = currentUserData["username"].(string)
	u.CodeSuggestionsEnabled = ideData["codeSuggestionsEnabled"].(bool)

	return nil
}
