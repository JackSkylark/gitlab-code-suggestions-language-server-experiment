package api

import (
	"fmt"
)

type PersonalAccessToken struct {
	Name   string   `json:"name"`
	Scopes []string `json:"scopes"`
}

var requiredPATScope = "api"

func (p PersonalAccessToken) IsValid() (bool, string) {
	valid := false
	tokenState := "invalid"
	message := fmt.Sprintf("has scope(s) '%v' (needs '%v')", p.Scopes, requiredPATScope)

	if p.hasSuitableScopes() {
		valid = true
		tokenState = "valid"
	}

	return valid, fmt.Sprintf("Token '%s' is %s - %s", p.Name, tokenState, message)
}

func (p PersonalAccessToken) hasSuitableScopes() bool {
	for _, i := range p.Scopes {
		if i == requiredPATScope {
			return true
		}
	}

	return false
}
