package token_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/api"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

const tokenName = "fake-token"

func TestRunCheck(t *testing.T) {
	type args struct {
		patResponse  string
		userResponse string
	}
	tests := []struct {
		name    string
		args    args
		want    *api.User
		wantErr error
	}{
		{
			"Valid",
			args{`{ "name": "fake-token", "scopes": ["api"] }`, `{ "data": { "currentUser": { "username": "joe", "ide": { "codeSuggestionsEnabled": true } } } }`},
			&api.User{Username: "joe", CodeSuggestionsEnabled: true, PersonalAccessToken: api.PersonalAccessToken{Name: "fake-token", Scopes: []string{"api"}}},
			nil,
		},
		{
			"Broken",
			args{``, ``},
			nil,
			errors.New("unexpected end of JSON input"),
		},
		{
			"Invalid",
			args{`{}`, `{}`},
			nil,
			errors.New("JSON missing data"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := setupHTTPServer(t, tt.args.patResponse, tt.args.userResponse)
			defer ts.Close()

			got, err := token.RunCheck(tokenName, ts.URL)
			if err != nil {
				if tt.wantErr != nil {
					require.ErrorContains(t, err, tt.wantErr.Error())
				} else {
					require.FailNow(t, err.Error())
				}
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func setupHTTPServer(t *testing.T, patResponse, userResponse string) *httptest.Server {
	t.Helper()

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v4/personal_access_tokens/self", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(patResponse))
		if err != nil {
			t.Fatal(err)
		}
	})
	mux.HandleFunc("/api/graphql", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte(userResponse))
		if err != nil {
			t.Fatal(err)
		}
	})

	return httptest.NewServer(mux)
}
