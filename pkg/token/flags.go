package token

import (
	"github.com/urfave/cli/v2"
)

const (
	LangSrvGitLabAPITokenParamName  = "gitlab-api-token"
	LangSrvGitLabAPITokenEnvVarName = "LANGSRV_GITLAB_API_TOKEN"

	baseURLParamName       = "gitlab-url"
	baseURLParamEnvVarName = "LANGSRV_GITLAB_URL"
)

var gitlabAPITokenParam = &cli.StringFlag{
	Name:     LangSrvGitLabAPITokenParamName,
	Usage:    "GitLab API token",
	Value:    "",
	EnvVars:  []string{LangSrvGitLabAPITokenEnvVarName},
	FilePath: GitlabAccessTokenFile(),
}

var baseURLParam = &cli.StringFlag{
	Name:    baseURLParamName,
	Usage:   "The base URL for GitLab",
	Value:   "https://gitlab.com",
	EnvVars: []string{baseURLParamEnvVarName},
}

func TokenCheckFlags() []cli.Flag {
	return []cli.Flag{gitlabAPITokenParam, baseURLParam}
}
