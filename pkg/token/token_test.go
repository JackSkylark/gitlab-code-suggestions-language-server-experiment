package token_test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

const testPAT = "glpat-deadbeefdeadbeefdead"

func TestWriteAccessToken(t *testing.T) {
	cfgDir := mockUserConfigDir(t)

	require.NoError(t, token.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(filepath.Join(cfgDir, "gitlab-ls", "token"))
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func TestWriteAccessToken_FileExists(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	tokenFile := filepath.Join(cfgDir, "gitlab-ls", "token")
	require.NoError(t, os.MkdirAll(filepath.Dir(tokenFile), 0o755))
	require.NoError(t, os.WriteFile(tokenFile, []byte("glpat-c0ffeec0ffeec0ffeec0ff"), 0o600))

	require.NoError(t, token.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(filepath.Join(cfgDir, "gitlab-ls", "token"))
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func TestWriteAccessToken_Invalid(t *testing.T) {
	cfgDir := mockUserConfigDir(t)

	require.ErrorIs(t, token.WriteGitlabAccessToken("lolwut?"), token.ErrInvalidAccessToken)

	require.NoFileExists(t, filepath.Join(cfgDir, "gitlab-ls", "token"))
}

func TestWriteAccessToken_TokenFileLocationFromEnv(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	envTokenFile := filepath.Join(cfgDir, "my-custom-gitlab-token-file")

	t.Setenv("LANGSRV_GITLAB_API_TOKEN_FILE", envTokenFile)

	require.NoError(t, token.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(envTokenFile)
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))

	require.NoFileExists(t, filepath.Join(cfgDir, "gitlab-ls", "token"))
}
