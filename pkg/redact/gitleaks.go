package redact

import (
	_ "embed"
	"fmt"
	"regexp"
	"strings"

	"github.com/BurntSushi/toml"
)

//go:embed gitleaks-9c6650d.toml
var gitleaksConfig string

// GitleaksRule for redacting secrets.
type GitleaksRule struct {
	SecretGroup   int
	Regex         string
	CompiledRegex *regexp.Regexp
	Keywords      []string
}

// Redact secrets in raw string.
func (r *GitleaksRule) Redact(raw string) (string, bool) {
	if !r.keywordHit(raw) {
		return raw, false
	}

	secrets := r.CompiledRegex.FindAllStringSubmatch(raw, -1)
	if len(secrets) == 0 {
		return raw, false
	}

	redacted := raw

	for _, secret := range secrets {
		secretStr := secret[r.SecretGroup]
		mask := strings.Repeat("*", len(secretStr))
		redacted = strings.ReplaceAll(redacted, secretStr, mask)
	}

	return redacted, true
}

func (r *GitleaksRule) keywordHit(raw string) bool {
	if len(r.Keywords) == 0 {
		return true
	}

	for _, kw := range r.Keywords {
		if strings.Contains(raw, kw) {
			return true
		}
	}

	return false
}

// LoadGitleaksRules parses embedded Gitleaks configuration and returns a slice
// of secrets detection rules.
func LoadGitleaksRules() ([]*GitleaksRule, error) {
	cfg := struct {
		Rules []*GitleaksRule
	}{}

	if _, err := toml.Decode(gitleaksConfig, &cfg); err != nil {
		return nil, fmt.Errorf("parsing embedded Gitleaks TOML configuration: %w", err)
	}

	if len(cfg.Rules) == 0 {
		return nil, fmt.Errorf("no Gitleaks rules loaded from embedded configuration")
	}

	for i, rule := range cfg.Rules {
		expr, err := regexp.Compile(rule.Regex)
		if err != nil {
			return nil, fmt.Errorf("compiling regexp for rule at index %d: %w", i, err)
		}

		cfg.Rules[i].CompiledRegex = expr
	}

	return cfg.Rules, nil
}
