package cmd

import (
	"fmt"
	"strings"

	"github.com/urfave/cli/v2"
)

const (
	TimeoutFlagName         = "timeout-seconds"
	RedactSensitiveFlagName = "redact"
	LogFileFlagName         = "log-file"
	DebugFlagName           = "debug"
	QuietFlagName           = "quiet"
	GitLabURLFlagName       = "gitlab-url"

	EnvPrefix = "LANGSRV"
)

var TimeoutFlag = &cli.UintFlag{ // nolint:gochecknoglobals
	Name:    TimeoutFlagName,
	Usage:   "GitLab API request timeout (seconds)",
	Value:   10,
	EnvVars: []string{AsEnvVar(TimeoutFlagName)},
}

var RedactSensitiveFlag = &cli.BoolFlag{ // nolint:gochecknoglobals
	Name:    RedactSensitiveFlagName,
	Usage:   "redact secrets and other sensitive content in code",
	Value:   true,
	EnvVars: []string{AsEnvVar(RedactSensitiveFlagName)},
}

var LogFileFlag = &cli.StringFlag{ // nolint:gochecknoglobals
	Name:    LogFileFlagName,
	Usage:   "log output to file instead of stdout",
	Value:   "",
	EnvVars: []string{AsEnvVar(LogFileFlagName)},
}

var DebugFlag = &cli.BoolFlag{ // nolint:gochecknoglobals
	Name:    DebugFlagName,
	Usage:   "output debugging information",
	Value:   false,
	EnvVars: []string{AsEnvVar(DebugFlagName)},
}

var QuietFlag = &cli.BoolFlag{ // nolint:gochecknoglobals
	Name:    QuietFlagName,
	Usage:   "only output warnings and errors",
	Value:   false,
	EnvVars: []string{AsEnvVar(QuietFlagName)},
}

// ServeFlags for [RunServe] command action.
func ServeFlags() []cli.Flag {
	return []cli.Flag{TimeoutFlag, RedactSensitiveFlag, LogFileFlag, DebugFlag, QuietFlag}
}

// AsEnvVar converts a string to be suitable for an environment variable name.
//
// Uppercases letters, converts all dashes to underscores, and adds a prefix
// to avoid conflicts with other environment variables.
func AsEnvVar(s string) string {
	return fmt.Sprintf("%s_%s", EnvPrefix, strings.ToUpper(strings.ReplaceAll(s, "-", "_")))
}
