package cmd

import (
	"fmt"

	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/editor-extensions/experiments/gitlab-code-suggestions-language-server-experiment/pkg/token"
)

func RunGitlabTokenRegister(cli *cli.Context) error {
	return token.RunRegister()
}

func RunGitlabTokenCheck(cCtx *cli.Context) error {
	t := cCtx.String(token.LangSrvGitLabAPITokenParamName)
	if t == "" {
		return fmt.Errorf("no GitLab API Token specified")
	}

	user, err := token.RunCheck(t, cCtx.String(GitLabURLFlagName))
	if err != nil {
		return err
	}

	_, explanation := user.IsTokenValidExplanation()

	fmt.Println(explanation)

	return err
}
