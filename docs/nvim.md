# NeoVim Configuration

Experimental configuration for our Neovim has been removed in favor of our plugin for Neovim. See the [`gitlab.vim`](https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim) project for documentation on getting started.
