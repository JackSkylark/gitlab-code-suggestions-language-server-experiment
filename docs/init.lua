local function report_error(message)
  vim.notify("Error: " .. message, vim.log.levels.ERROR)
end

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end

local os = ''
if vim.fn.has('macunix') then
  os = 'darwin'
elseif vim.fn.has('linux') then
  os = 'linux'
end

if os == '' then
  report_error('no os detected')
end

local stampfile = vim.fn.stdpath('data') .. '/stampfile'
local basename = 'gitlab-code-suggestions-language-server-experiment'
local lsbinfilename = basename .. '-' .. os .. '-amd64'

local targetfile = ''

if not(vim.loop.fs_stat(stampfile)) then
  -- download language server binary
  local latest_version_list = vim.fn.systemlist({
    '/bin/bash',
    '-c',
    'curl --fail -s -L https://gitlab.com/api/v4/projects/46261736/releases/permalink/latest | jq -r .name'
  })

  if not(vim.v.shell_error == 0) then
     report_error('unable to identify latest version')
  end

  local version = latest_version_list[1]
  local oslabel = ''
  if vim.fn.has('macunix') then
     oslabel = 'darwin'
  elseif vim.fn.has('linux') then
     oslabel = 'linux'
  end

  if oslabel == '' then
     report_error('no os detected')
  end

  local lsbinurl = 'https://gitlab.com/api/v4/projects/46261736/packages/generic/releases/' .. version .. '/' .. lsbinfilename
  targetfile = vim.fn.stdpath("data") .. '/' .. lsbinfilename

  if not vim.loop.fs_stat(targetfile) then
    vim.print('downloading ' .. lsbinurl .. ' to ' .. targetfile)
    vim.fn.system({
      "curl",
      lsbinurl,
      "--output",
      targetfile
    })
  end

  -- set permissions
  if os == 'darwin' or os == 'linux' then
    vim.fn.system({
      'chmod',
      '+x',
      targetfile
    })
    if os == 'darwin' then
      vim.fn.system({
        'xattr',
        '-d',
        'com.apple.quarantine',
        targetfile
      })
    end
  end

  local f = io.open(stampfile, 'w')
  if f == nil then
    report_error('unable to open ' .. stampfile)
  else
    f:write(targetfile)
    f:close()
  end
else
  local f = io.open(stampfile, 'r')
  if f == nil then
    report_error('unable to open ' .. stampfile)
  else
    targetfile = f:read("*line")
    f:close()
  end
end

vim.print('language server binary located at ' .. targetfile)

-- register GitLab token directly through NeoVim
function GitLabRegisterToken(token)
   vim.fn.system({
      '/bin/bash',
      '-c',
      'echo ' .. token .. ' | ' .. targetfile .. ' token register'
  })
  if vim.v.shell_error == 0 then
     vim.print("token registered")
  else
     report_error('unable to set token')
  end
end

vim.api.nvim_create_user_command('GitLabRegisterToken',
  function(opts)
    GitLabRegisterToken(opts.fargs[1])
  end,
  { nargs = 1 }
)

-- install plugins nvim-lspconfig (lsp configuration package) and nvim-cmp (Auto-completion)
vim.opt.rtp:prepend(lazypath)
require('lazy').setup({
  { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
  },

  { -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = { 'hrsh7th/cmp-nvim-lsp' },
  },
}, {})

local cmp = require 'cmp'
cmp.setup {
  mapping = cmp.mapping.preset.insert {
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end, { 'i', 's' }),
  },
  sources = {
    { name = 'nvim_lsp' },
  },
}

local cmd = {
  targetfile,
  'serve',
  '--name',
  basename,
  '--srcdir',
  vim.fn.getcwd(),
  '--timeout-seconds',
  '300'
}

local configs = require 'lspconfig.configs'
local util = require 'lspconfig.util'

-- Check if the config is already defined (useful when reloading this file)
if not configs.gitlab_ai_code_suggestions then
  configs.gitlab_ai_code_suggestions = {
    default_config = {
      cmd = cmd,
      filetypes = {'python', 'ruby'},
      root_dir = function(fname)
      local root_files = {
        'pyproject.toml',
        'setup.py',
        'setup.cfg',
        'requirements.txt',
        'Pipfile',
        'Gemfile',
      }
      return util.root_pattern(unpack(root_files))(fname) or util.find_git_ancestor(fname)
    end,
    single_file_support = true,
    settings = {},
    },
  }
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

configs.gitlab_ai_code_suggestions.setup{
  capabilities = capabilities
}
