# Code Suggestions (Beta)

Write code more efficiently by using generative AI to suggest code while you’re developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-in-vs-code)

This feature is in
[Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta)
Code Suggestions is a generative artificial intelligence (AI) model. GitLab currently leverages [Google Cloud’s Vertex AI Codey API models](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview)

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Google Vertex AI Codey APIs Data Governance](https://cloud.google.com/vertex-ai/docs/generative-ai/data-governance)

# Emacs Configuration

![](./emacs.gif)

For setting up Emacs, make sure the [lsp-mode](https://github.com/emacs-lsp/lsp-mode) package is installed. You can
go to the LSP configuration by adding the following snippet to your
configuration under $HOME/config. Depending on your setting, you can
parameterize `<project-dir>` to select the project you are currently viewing in
emacs.

```lisp
(after! lsp-mode
  (lsp-register-client
   (make-lsp-client
    :new-connection
    (lsp-stdio-connection
    	'("opt/gitlab-code-suggestions-language-server-experiment"
    	"serve"
        "--name"
        "gitlab-code-suggestions-language-server-experiment"
        "--srcdir"
        "<project-dir>"
        "--timeout-seconds"
        "100"
        )
     )
    :major-modes '(python-mode)
    :server-id 'gitlab-code-suggestions-language-server-experiment)))
```

Beta users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations)